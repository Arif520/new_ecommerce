<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NewShopController extends Controller
{
    public function index(){
        return view('frontEnd.home.home');
    }

    public function categoryKitchen(){

        return view('frontEnd.category.kitchen.kitchen');
    }

    public function categoryHousehold(){

        return view('frontEnd.category.household.household');
    }
}



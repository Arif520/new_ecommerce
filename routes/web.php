<?php

Route::get('/',[
    'uses' =>'NewShopController@index',
    'as'   =>'/'
]);

Route::get('/kitchen',[
    'uses' =>'NewShopController@categoryKitchen',
    'as'   =>'category-kitchen'
]);

Route::get('/household',[
    'uses' =>'NewShopController@categoryHousehold',
    'as'   =>'category-household'
]);